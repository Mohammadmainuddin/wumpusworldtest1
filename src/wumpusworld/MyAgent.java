package wumpusworld;

/**
 * Contains starting code for creating your own Wumpus World agent.
 * Currently the agent only make a random decision each turn.
 * 
 * @author Johan Hagelbäck
 */
public class MyAgent implements Agent
{
    private World w;
    int rnd;
    
    /**
     * Creates a new instance of your solver agent.
     * 
     * @param world Current world state 
     */
    public MyAgent(World world)
    {
        w = world;   
    }
   
            
    /**
     * Asks your solver agent to execute an action.
     */

    public void doAction()
    {   
        //Checking if the game started right now. If so, then initialize the valuse of MyAgent Utility
        if(!w.isVisited(1, 2) && !w.isVisited(2, 1)){
            if(w.hasArrow()){
                MyAgentUtility.initialize();
            }
            if(w.hasStench(1,1) && w.hasArrow()){
                turnDirection(1);
                w.doAction(World.A_SHOOT);                
                MyAgentUtility.printValues();
                System.out.println("An arrow just shoot");
                System.out.println();
                return;
            }
        }
        
        //Location of the player
        int cX = w.getPlayerX();
        int cY = w.getPlayerY();

        //System.out.println(cX + " " + cY + " " + MyAgentUtility.getAdjacentNodeNumber(cX, cY));
        
        //Basic action:
        //Grab Gold if we can.
        if (w.hasGlitter(cX, cY))
        {
            w.doAction(World.A_GRAB);
            return;
        }
        
        //Basic action:
        //We are in a pit. Climb up.
        if (w.isInPit())
        {
            w.doAction(World.A_CLIMB);
            return;
        }
        
        if (w.hasBreeze(cX, cY)){
            MyAgentUtility.setPit(cX,cY);   //Adjacent positions has marked as there could be a pit.
        }else{
            MyAgentUtility.setNoPit(cX,cY);   //Adjacent positions has marked as no pit here.
        }
        
        if(w.hasStench(cX, cY)){
            MyAgentUtility.setWumpus(cX,cY);//Adjacent positions has marked as not wumpus here.
            int tempDirection = MyAgentUtility.commonWumpusDirection(cX, cY);
            if(tempDirection > 0 && w.hasArrow()){
                turnDirection(tempDirection);
                w.doAction(World.A_SHOOT);
                MyAgentUtility.printValues();
                System.out.println("An arrow just shoot");
                System.out.println();
                return;
            }
        }else{
            MyAgentUtility.setNoWumpus(cX,cY);//Adjacent positions has marked as there could be a wumpus.
        }
        
        // Show values what has already found from that position.
        MyAgentUtility.printValues();
        
        if(!MyAgentUtility.needToGoDestination){
            MyAgentUtility.getSafePosition(w, cX, cY);
        }
        turnDirection(MyAgentUtility.getDirection(w, cX,cY));
        w.doAction(World.A_MOVE);

        //return;

        
        /*
        //Test the environment
        if (w.hasBreeze(cX, cY))
        {
            System.out.println("I am in a Breeze");
        }
        if (w.hasStench(cX, cY))
        {
            System.out.println("I am in a Stench");
        }
        if (w.hasPit(cX, cY))
        {
            System.out.println("I am in a Pit");
        }
        if (w.getDirection() == World.DIR_RIGHT)
        {
            System.out.println("I am facing Right");
        }
        if (w.getDirection() == World.DIR_LEFT)
        {
            System.out.println("I am facing Left");
        }
        if (w.getDirection() == World.DIR_UP)
        {
            System.out.println("I am facing Up");
        }
        if (w.getDirection() == World.DIR_DOWN)
        {
            System.out.println("I am facing Down");
        }
        
        //decide next move
        rnd = decideRandomMove();
        if (rnd==0)
        {
            w.doAction(World.A_TURN_LEFT);
            w.doAction(World.A_MOVE);
        }
        
        if (rnd==1)
        {
            w.doAction(World.A_MOVE);
        }
                
        if (rnd==2)
        {
            w.doAction(World.A_TURN_LEFT);
            w.doAction(World.A_TURN_LEFT);
            w.doAction(World.A_MOVE);
        }
                        
        if (rnd==3)
        {
            w.doAction(World.A_TURN_RIGHT);
            w.doAction(World.A_MOVE);
        }
        
        */
                
    }    
    
     /**
     * Genertes a random instruction for the Agent.
     */
    public int decideRandomMove()
    {
      return (int)(Math.random() * 4);
    }
    
    private void turnLeft(){
        
        if (w.getDirection() == World.DIR_UP)
        {
            w.doAction(World.A_TURN_LEFT);
            return;
        }
        
        if (w.getDirection() == World.DIR_DOWN)
        {
            w.doAction(World.A_TURN_RIGHT);
            return;
        }
                
        if (w.getDirection() == World.DIR_RIGHT)
        {
            w.doAction(World.A_TURN_LEFT);
            w.doAction(World.A_TURN_LEFT);
            return;
        }
    }
    
    private void turnRight(){
        
        if (w.getDirection() == World.DIR_DOWN)
        {
            w.doAction(World.A_TURN_LEFT);
            return;
        }
        
        if (w.getDirection() == World.DIR_UP)
        {
            w.doAction(World.A_TURN_RIGHT);
            return;
        }
                
        if (w.getDirection() == World.DIR_LEFT)
        {
            w.doAction(World.A_TURN_LEFT);
            w.doAction(World.A_TURN_LEFT);
            return;
        }
    }
    
    private void turnUp(){
        
        if (w.getDirection() == World.DIR_RIGHT)
        {
            w.doAction(World.A_TURN_LEFT);
            return;
        }
        
        if (w.getDirection() == World.DIR_LEFT)
        {
            w.doAction(World.A_TURN_RIGHT);
            return;
        }
                
        if (w.getDirection() == World.DIR_DOWN)
        {
            w.doAction(World.A_TURN_LEFT);
            w.doAction(World.A_TURN_LEFT);
            return;
        }
    }
    
    private void turnDown(){
        
        if (w.getDirection() == World.DIR_LEFT)
        {
            w.doAction(World.A_TURN_LEFT);
            return;
        }
        
        if (w.getDirection() == World.DIR_RIGHT)
        {
            w.doAction(World.A_TURN_RIGHT);
            return;
        }
                
        if (w.getDirection() == World.DIR_UP)
        {
            w.doAction(World.A_TURN_LEFT);
            w.doAction(World.A_TURN_LEFT);
            return;
        }
    }
    
    //Returns an integer value which corresponds to a direction.
    // 1 for up, 2 for right, 3 for down, 4 for left.
    public int getIntDirection(){
        int dir = 1;        
        if(w.getDirection() == World.DIR_RIGHT){
            dir = 2;
        }
        if(w.getDirection() == World.DIR_DOWN){
            dir = 3;
        }
        if(w.getDirection() == World.DIR_LEFT){
            dir = 4;
        }        
        return dir;
    }
    
    public void turnDirection(int dir){
        if(dir == 1){
            turnUp();
        }
        if(dir == 2){
            turnRight();
        }
        if(dir == 3){
            turnDown();
        }
        if(dir == 4){
            turnLeft();
        }
    }
}