/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wumpusworld;

/**
 *
 * @author Snigdho
 */
public class MyAgentUtilityNode {
    public int wumpus; // 0 means no wumpus, -1 means unknown or has not come near this position, higher the number is bigger probability that there is a wumpus
    public int pit; // 0 means no pit, -1 means unknown or has not come near this position, higher the number is bigger probability that there is a pit.
    public int dangerCount;
    public boolean scouted;

    public MyAgentUtilityNode() {
        this.wumpus = -1;
        this.pit = -1;
        this.dangerCount = 50;
        this.scouted = false;
    }
    
    public void updateDangerCount(){
        this.dangerCount = 0;
        if(this.wumpus > 0){
            this.dangerCount = this.dangerCount + this.wumpus;
        }
        if(this.pit > 0){
            this.dangerCount = this.dangerCount + this.pit;
        }
    }
}
