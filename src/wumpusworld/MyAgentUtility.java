/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wumpusworld;

/**
 *
 * @author Snigdho
 */
public class MyAgentUtility {
    public static MyAgentUtilityNode [][] a = new MyAgentUtilityNode [5][5];
    public static int desX; // represents X-axis value of destination position
    public static int desY; // represents Y-axis value of destination position
    public static int preX; // represents X-axis value of previous position
    public static int preY; // represents Y-axis value of previous position
    public static boolean needToGoDestination;
    
    // Initializing values for the first time;
    // Did not use any constructor but a function. Beecause no object is going to be created but the static values will be initialized;
    public static void initialize(){
        int i,j;        
        for(i=1; i<=4; i++){
            for(j=1; j<=4; j++){
                a[j][i] = new MyAgentUtilityNode();
            }
        }
        a[1][1].pit = 0;
        a[1][1].wumpus = 0;
        a[1][1].dangerCount = 0;
        desX = 1;
        desY = 2;        
        preX = -1;
        preY = -1;
        System.out.println("Initializing values for the first time.");
    }
    
    
    //print values after testting the environment
    public static void printValues(){
        int i,j;        
        for(i=4; i>=1; i--){
            for(j=1; j<=4; j++){
                System.out.print(j+" "+i+" "+a[j][i].pit+" "+a[j][i].wumpus+" "+a[j][i].dangerCount+"\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    
    // if a breeze is found
    public static void setPit(int x, int y){
        if(a[x][y].scouted){
            return;
        }
            
        if(x != 1 && a[(x-1)][y].pit != 0){
            a[(x-1)][y].pit += 2;
            a[(x-1)][y].updateDangerCount();
        }
        if(x != 4 && a[(x+1)][y].pit != 0){
            a[(x+1)][y].pit += 2;
            a[(x+1)][y].updateDangerCount();
        }
        if(y != 1 && a[x][(y-1)].pit != 0){
            a[x][(y-1)].pit += 2;
            a[x][(y-1)].updateDangerCount();
        }
        if(y != 4 && a[x][(y+1)].pit != 0){
            a[x][(y+1)].pit += 2;
            a[x][(y+1)].updateDangerCount();
        }
        
        // for corner places
        if(a[4][4].pit == 3){
            a[4][4].pit += 2;
            a[4][4].updateDangerCount();
        }
        if(a[1][4].pit == 3){
            a[1][4].pit += 2;
            a[1][4].updateDangerCount();
        }
        if(a[4][1].pit == 3){
            a[4][1].pit += 2;
            a[4][1].updateDangerCount();
        }
    }
    
    
    // if a stench is found
    public static void setWumpus(int x, int y){
        if(a[x][y].scouted){
            return;
        }
        
        if(x != 1 && a[(x-1)][y].wumpus != 0){
            a[(x-1)][y].wumpus += 10;            
            a[(x-1)][y].updateDangerCount();
        }
        if(x != 4 && a[(x+1)][y].wumpus != 0){
            a[(x+1)][y].wumpus += 10;
            a[(x+1)][y].updateDangerCount();
        }
        if(y != 1 && a[x][(y-1)].wumpus != 0){
            a[x][(y-1)].wumpus += 10;
            a[x][(y-1)].updateDangerCount();
        }
        if(y != 4 && a[x][(y+1)].wumpus != 0){
            a[x][(y+1)].wumpus += 10;
            a[x][(y+1)].updateDangerCount();
        }
        
        a[x][y].scouted = true;
    }
    
    
    //if no breeze is found
    public static void setNoPit(int x, int y){           
        if(x != 1){
            a[(x-1)][y].pit = 0;
            a[(x-1)][y].updateDangerCount();
        }
        if(x != 4){
            a[(x+1)][y].pit = 0;
            a[(x+1)][y].updateDangerCount();
        }
        if(y != 1){
            a[x][(y-1)].pit = 0;
            a[x][(y-1)].updateDangerCount();
        }
        if(y != 4){
            a[x][(y+1)].pit = 0;
            a[x][(y+1)].updateDangerCount();
        }
    }
    
    
    //if no stench is found
    public static void setNoWumpus(int x, int y){           
        if(x != 1){
            a[(x-1)][y].wumpus = 0;
            a[(x-1)][y].updateDangerCount();
        }
        if(x != 4){
            a[(x+1)][y].wumpus = 0;
            a[(x+1)][y].updateDangerCount();
        }
        if(y != 1){
            a[x][(y-1)].wumpus = 0;
            a[x][(y-1)].updateDangerCount();
        }
        if(y != 4){
            a[x][(y+1)].wumpus = 0;
            a[x][(y+1)].updateDangerCount();
        }
        
        a[x][y].scouted = true;        
    }
    
    
    //finding the most safe position
    public static void getSafePosition(World ww, int xx, int yy){
        int x = 0,y = 0,i,j; 
        int minDanger = 500;
        int minDistance = 500;        
        
        for(i=1; i<=4; i++){
            for(j=1; j<=4; j++){
                if(ww.isVisited(j, i)){
                    continue;
                }
                if(a[j][i].dangerCount<minDanger){
                    x = j;
                    y = i;
                    minDanger = a[j][i].dangerCount;
                }
            }
        }       
        
        for(i=4; i>=1; i--){
            for(j=1; j<=4; j++){
                if(ww.isVisited(j, i)){
                    continue;
                }
                int tempDistance = getDistance(j, i, xx, yy);                
                if(a[j][i].dangerCount == minDanger && tempDistance < minDistance){
                    x = j;
                    y = i;
                    minDistance = tempDistance;
                }
            }
        }
        desX = x;
        desY = y;
        
        System.out.println(minDanger + "    " +desX+ " " +desY+ "  ");
        System.out.println();
        needToGoDestination = true;
        preX = xx;
        preY = yy;
    }
    
    
    // getting distance of two points
    // or, number of moves required to reach from one position to another
    public static int getDistance(int x1, int y1, int x2, int y2){
        int distance = 0;
        int xx,yy;
        xx = x1 - x2;
        if(xx < 0){
            xx = xx * (-1);
        }
        yy = y1 - y2;
        if(yy < 0){
            yy = yy * (-1);
        }
        distance = xx + yy;
        return distance;
    }
    
    
    // getting direction to reach the most safe position
    public static int getDirection(World ww,int xx, int yy){
        int dir, distance, tempDirScore, dirScore;  // position with lowest dirScore (Direction score) will be chosen
        distance = getDistance(xx, yy, desX, desY);
                
        if(distance == 1){
            needToGoDestination = false;
            preX = -1;
            preY = -1;
            if(desX == xx && (desY - yy) == 1)
                return 1;
            if(desX == xx && (yy - desY) == 1)
                return 3;
            if(desY == yy && (desX - xx) == 1)
                return 2;
            if(desY == yy && (xx - desX) == 1)
                return 4;
        }
        
        //If the agent needs more than one move to reach the destination,
        //then the following lines will be executed;
        dirScore = 100;
        dir = 0;
        
        //checking if the next direction is up
        if(yy != 4 && ww.isVisited(xx, (yy+1))){
            tempDirScore = 0;
            if(preX == xx && preY == (yy + 1)){
                tempDirScore = 50;
            }            
            tempDirScore += getDistance(desX, desY, xx, (yy+1));
            
            if(tempDirScore < dirScore){
                dirScore = tempDirScore;
                dir = 1;
            }            
        }
        
        //checking if the next direction is down
        if(yy != 1 && ww.isVisited(xx, (yy-1))){
            tempDirScore = 0;
            if(preX == xx && preY == (yy-1)){
                tempDirScore = 50;
            }            
            tempDirScore += getDistance(desX, desY, xx, (yy-1));
            
            if(tempDirScore < dirScore){
                dirScore = tempDirScore;
                dir = 3;
            }            
        }
        
        //checking if the next direction is right
        if(xx != 4 && ww.isVisited((xx+1), yy)){
            tempDirScore = 0;
            if(preX == (xx+1) && preY == yy){
                tempDirScore = 50;
            }            
            tempDirScore += getDistance(desX, desY, (xx+1), yy);
            
            if(tempDirScore < dirScore){
                dirScore = tempDirScore;
                dir = 2;
            }            
        }
        
        //checking if the next direction is left
        if(xx != 1 && ww.isVisited((xx-1), yy)){
            tempDirScore = 0;
            if(preX == (xx-1) && preY == yy){
                tempDirScore = 50;
            }            
            tempDirScore += getDistance(desX, desY, (xx-1), yy);
            
            if(tempDirScore < dirScore){
                dirScore = tempDirScore;
                dir = 4;
            }            
        }
        preX = xx;
        preY = yy;
        return dir;
    }
    
    
    //if any wumpus has found twice for a position
    public static int commonWumpusDirection(int xx, int yy){
        
            if(yy != 4){
                if(a[xx][(yy+1)].dangerCount >= 19){                    
                    return 1;
                }                    
            }
            if(yy != 1){
                if(a[xx][(yy-1)].dangerCount >= 19){                    
                    return 3;
                }                    
            }
            if(xx != 4){
                if(a[(xx+1)][yy].dangerCount >= 19){
                    return 2;
                }                    
            }
            if(xx != 1){
                if(a[(xx-1)][yy].dangerCount >= 19){
                    return 4;
                }                    
            }            
        return 0;
    }     
}